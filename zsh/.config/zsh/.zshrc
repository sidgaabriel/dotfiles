# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Export zdotdir folder
export ZDOTDIR=$HOME/.config/zsh
export ZDOTCONFIG=$HOME/.config/zsh/config

# Load zinit plugin
source "$ZDOTDIR/zinit"

# Load zsh-functions helper
source "$ZDOTCONFIG/functions"

# Add aliases, export files
zsh_add_file "aliases"
zsh_add_file "exports"
zsh_add_file "plugins"
zsh_add_file "prompt"
zsh_add_file "nvim-switch"

# Enable emacs mode
bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward

# History
HISTFILE=~/.cache/zsh/history
SAVEHIST=1000000
HISTSIZE=1000000
HISTDUP=erase

# some useful options (man zshoptions)
setopt append_history
setopt sharehistory
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_save_no_dups
setopt hist_ignore_space
setopt hist_find_no_dups

# Completion Styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no 
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Shell integration's
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"

#stty stop undef		# Disable ctrl-s to freeze terminal.
zle_highlight=('paste:none')

# beeping is annoying
unsetopt BEEP

# Load zsh-completions
autoload -U compinit && compinit
zinit cdreplay -q
