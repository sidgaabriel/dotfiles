----------------------------
-- gabs neovim lua config --
----------------------------
local g = vim.g
local c = vim.cmd

g.scratchpad_autostart = 0
g.scratchpad_autosize = 1
g.scratchpad_location = '~/.local/share/nvim/.scratchpad'
g.scratchpad_autofocus = 1

-- Size window
-- g.scratchpad_textwidth = 50
-- g.scratchpad_minwidth = 50

-- Set font color
c('hi ScratchPad guifg=#98c379')
