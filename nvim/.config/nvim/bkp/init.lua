----------------------------
-- gabs neovim lua config --
----------------------------
require ("gabs.general")
require ("gabs.keys")
require ("gabs.plugins")
require ("gabs.bootstrap")
require ("gabs.highlights")
require ("gabs.scratchpad")
