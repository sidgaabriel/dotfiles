---------------------------
-- gabs neovim lua config --
----------------------------
local status, db = pcall(require, "dashboard")
db.setup({
	theme = 'doom',
	config = {
	header = {
	"",
	"",
	" ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗",
	" ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║",
	" ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║",
	" ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║",
	" ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║",
	" ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝",
	"",
	"",
},
preview = {
	file_height = 11,
	file_width = 70,
},
center = {
	{
		icon = " ",
		desc = " New file                                ",
		action = "ene!",
		shortcut = "SPC n",
	},
	{
		icon = "🗐  ",
		desc = "Find recent files                       ",
		action = "Telescope oldfiles",
		shortcut = "SPC f r",
	},
	{
		icon = "🖿  ",
		desc = "Find files                              ",
		action = "Telescope find_files find_command=rg,--hidden,--files",
		shortcut = "SPC f f",
	},
	{
		icon = "🗁  ",
		desc = "File browser                            ",
		action = "Telescope file_browser hidden=true",
		shortcut = "SPC f b",
	},
	{
		icon = "🗛  ",
		desc = "Find word                               ",
		action = "Telescope live_grep",
		shortcut = "SPC f g",
	},
	{
		icon = "🖉  ",
		desc = "Load new theme                          ",
		action = "Telescope colorscheme",
		shortcut = "SPC f c",
	},
},
footer = { "", "g a b s" }
}
})



